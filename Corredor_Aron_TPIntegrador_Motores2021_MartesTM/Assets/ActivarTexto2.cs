﻿using UnityEngine;
using UnityEngine.UI;

public class ActivarTexto2 : MonoBehaviour
{
    public Text textoInfo2;
    public Text textoDesaparecer;
    public Text Pokeballs;

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            mostrarTexto();
            ocultarTexto();
        }
    }

    private void mostrarTexto()
    {
        textoInfo2.text = "Encontra las 5 Pokeballs para abrir la puerta!";
    }

    private void ocultarTexto()
    {
        Destroy(textoDesaparecer);
    }
}
