﻿using UnityEngine;


public class DesactivarPared : MonoBehaviour
{
    [Header("Pared A Desactivar")]
    public GameObject ParedSoldados;

    [Header("Lasers Desactivados")]
    public GameObject Laser1;
    public GameObject Laser2;
    public GameObject Laser3;
    public GameObject Laser4;
    public GameObject Laser5;


    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            DesaparecerPared();
        }
    }

    private void DesaparecerPared()
    {
        Destroy(ParedSoldados);
        Destroy(Laser1);
        Destroy(Laser2);
        Destroy(Laser3);
        Destroy(Laser4);
        Destroy(Laser5);
    }

}
