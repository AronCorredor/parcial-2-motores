﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilitarBot : MonoBehaviour
{
    public GameObject Pared1;
    public GameObject Pared2;
    public GameObject Pared3;
    public GameObject Pared4;
    public GameObject Pared5;
    public static bool activoAlBoss;

    private void Start()
    {
        activoAlBoss = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            DestruirPared();
            activoAlBoss = true;
        }
    }

    private void DestruirPared()
    {
        Destroy(Pared1);
        Destroy(Pared2);
        Destroy(Pared3);
        Destroy(Pared4);
        Destroy(Pared5);        
    }
}
