﻿using UnityEngine;
using UnityEngine.UI;

public class Ganaste : MonoBehaviour
{
    [Header("Victoria Info")]
    public Text Victoria;


    [Header("Confetti Activar")]
    public GameObject Confetti3;
    public GameObject Confetti4;
    public GameObject Confetti5;
    public GameObject Confetti6;

    [Header("Audio Info")]
    public AudioSource controlSonido;
    public AudioClip sonidoVictoria;

    void Start()
    {

    }


    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ConfettiActivado();
            controlSonido.PlayOneShot(sonidoVictoria);
            mostrarTexto();
            Time.timeScale = 0;
        }
    }

    private void ConfettiActivado()
    {
        Confetti3.SetActive(true);
        Confetti4.SetActive(true);
        Confetti5.SetActive(true);
        Confetti6.SetActive(true);
    }

    private void mostrarTexto()
    {
        Victoria.text = "Felicitaciones, GANASTE! Apreta R para Reiniciar!";
    }
}
