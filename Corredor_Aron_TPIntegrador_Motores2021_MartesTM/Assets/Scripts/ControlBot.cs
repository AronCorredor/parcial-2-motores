﻿using UnityEngine;

public class ControlBot : MonoBehaviour
{
    private int hp;
    public Animator muerte;
    public Camera CamaraBala;
    public GameObject Proyectil;

    void Start()
    {
        hp = 100;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Proyectil"))
        {
            recibirdaño();
        }
    }
    public void recibirdaño()
    {
        hp = hp - 100;

        if (hp <= 0)
        {
            muerte.SetBool("Muerte", true);
            desaparecer();
        }
    }
    private void desaparecer()
    {
        Destroy(gameObject, 3);
    }
}
