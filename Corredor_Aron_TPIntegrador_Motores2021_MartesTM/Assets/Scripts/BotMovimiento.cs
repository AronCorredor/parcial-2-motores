﻿using UnityEngine;

public class BotMovimiento : MonoBehaviour
{
    public Rigidbody botRb;
    public Transform[] posicionBot;
    public float velocidadBot;

    private int posicionActual = 0;
    private int siguientePosicion = 1;

    void Update()
    {
        movPlataforma();
    }

    void movPlataforma()
    {
        botRb.MovePosition(Vector3.MoveTowards(botRb.position, posicionBot[siguientePosicion].position, velocidadBot * Time.deltaTime));

        if (Vector3.Distance(botRb.position, posicionBot[siguientePosicion].position) <= 0)
        {
            posicionActual = siguientePosicion;
            siguientePosicion++;

            if (siguientePosicion > posicionBot.Length - 1)
            {
                siguientePosicion = 0;
            }

        }
    }
}