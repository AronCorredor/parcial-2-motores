﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agacharse : MonoBehaviour
{
    public Animator agacharse;
    private bool activo;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            activo = !activo;
        }

        if (activo == true)
        {
            agacharse.SetBool("Agachado", true);
        }

        if (activo == false)
        {
            agacharse.SetBool("Agachado", false);
        }
    }
}
