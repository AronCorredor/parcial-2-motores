﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class JuegoControl : MonoBehaviour
{
    float tiempoRestate;
    public Text textoPerdiste;
    public GameObject player;
    public Rigidbody rb;

    void Start()
    {
        ComenzarJuego();
    }

    private void Update()
    {
        if (tiempoRestate == 0)
        {
            rb.GetComponent<Rigidbody>().position = new Vector3(42f, 0f, -3f);
        }
        setearTextos2();
    }
    public void ComenzarJuego()
    {
        StartCoroutine(ComenzarCronometro(300));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 300)
    {
        tiempoRestate = valorCronometro;
        while (tiempoRestate >= 0)
        {
            Debug.Log("Faltan " + tiempoRestate + " segundos!");
            yield return new WaitForSeconds(1.0f);
            tiempoRestate--;
        }
    }
    private void setearTextos2()
    {
        if (tiempoRestate <= 0)
        {
            textoPerdiste.text = "Mala suerte, Perdiste! Apreta R para volver a empezar!";
        }
    }
}
