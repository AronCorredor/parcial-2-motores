﻿using UnityEngine;

public class ActivarConfetti : MonoBehaviour
{
    [Header("Confetti Info")]
    public GameObject Confetti;
    public GameObject Confetti2;
    public GameObject Confetti3;
    public GameObject Confetti4;
    public GameObject Confetti5;
    public GameObject Confetti6;

    [Header("Audio Info")]
    public AudioSource controlSonido;
    public AudioClip sonidoFuegoArtificial;

    [Header("Musica Info")]
    public AudioClip sonidoMusica;
    public AudioSource controlSonido2;

    void Start()
    {

    }


    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ConfettiActivado();
            controlSonido.PlayOneShot(sonidoFuegoArtificial);
            controlSonido2.volume = 0;
        }
    }

    private void ConfettiActivado()
    {
        Confetti.SetActive(true);
        Confetti2.SetActive(true);
        Confetti3.SetActive(true);
        Confetti4.SetActive(true);
        Confetti5.SetActive(true);
        Confetti6.SetActive(true);

    }
}
