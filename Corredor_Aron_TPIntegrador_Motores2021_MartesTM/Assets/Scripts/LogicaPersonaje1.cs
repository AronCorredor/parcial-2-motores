﻿using UnityEngine;

public class LogicaPersonaje1 : MonoBehaviour
{
    public float velocidadRotacion = 200.0f;

    private Animator anim;

    public float x, y;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
    }
}
