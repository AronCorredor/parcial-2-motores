﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Reset : MonoBehaviour
{
    public Cronometro cronometro;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1);
            cronometro.enabled = true;
        }
        else if (gameObject.transform.position.y <= -16)
        {
            SceneManager.LoadScene(1);
            cronometro.enabled = true;
        }

    }

}

