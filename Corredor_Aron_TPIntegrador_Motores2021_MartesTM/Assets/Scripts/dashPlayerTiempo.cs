﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dashPlayerTiempo : MonoBehaviour
{
    public float dashSpeed;
    bool isDashing;
    Rigidbody rb;
    private float dashRateTime = 0;

    public GameObject dashEffect;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            isDashing = true;
        }
    }

    private void FixedUpdate()
    {
        if (isDashing)
        {
            Dashing();
        }
    }

    private void Dashing()
    {
        if (Time.time > dashRateTime)
        {
            rb.AddForce(transform.forward * dashSpeed, ForceMode.Impulse);
            isDashing = false;

            dashRateTime = Time.time + dashSpeed;

            GameObject effect = Instantiate(dashEffect, Camera.main.transform.position, dashEffect.transform.rotation);
            effect.transform.parent = Camera.main.transform;
            effect.transform.LookAt(transform);
        }
    }
}