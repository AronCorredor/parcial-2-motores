﻿using UnityEngine;

public class Disparo : MonoBehaviour
{
    [Header("Informacion")]
    public GameObject Proyectil;
    public Transform spawnPoint;
    public Transform armaBoca;
    public GameObject efectoFlash;
    public AudioSource controlSonido;
    public AudioClip sonidoDisparo;

    [Header("Fuerzas")]
    public float shotForce = 1500;
    public float shotRate = 0.3f;

    private float shotRateTime = 0;

    void Start()
    {

    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time > shotRateTime)
            {
                GameObject newBullet;

                newBullet = Instantiate(Proyectil, spawnPoint.position, spawnPoint.rotation);

                newBullet.GetComponent<Rigidbody>().AddForce(spawnPoint.forward * shotForce);

                shotRateTime = Time.time + shotRate;

                controlSonido.PlayOneShot(sonidoDisparo);

                GameObject flashClone = Instantiate(efectoFlash, armaBoca.position, Quaternion.Euler(armaBoca.forward), transform);
                Destroy(flashClone, 1f);

                Destroy(newBullet, 1);
            }

        }
    }
}
