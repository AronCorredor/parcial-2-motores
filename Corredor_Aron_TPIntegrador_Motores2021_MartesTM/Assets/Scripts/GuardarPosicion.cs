﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GuardarPosicion : MonoBehaviour
{

    public GuardarPosicion instancia;

    public DataPersistencia data;

    string saveData = "save.dat";

    public GameObject _player;


    private void Awake()
    {
        if (instancia is null)
        {
            DontDestroyOnLoad(this.gameObject);
            instancia = this;
        }
        else if (instancia != this)
            Destroy(this.gameObject);

        CargarData();

    }

    void Start()
    {

    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            GuardarData();
        }
    }

    public void GuardarData()
    {


        data.posicionJugador = new Punto(_player.transform.position);

        string filePath = Application.persistentDataPath + "/" + saveData;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos guardados");




    }

    public void CargarData()
    {
        string filePath = Application.persistentDataPath + "/" + saveData;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            _player.transform.position = new Vector3(data.posicionJugador.x, data.posicionJugador.y, data.posicionJugador.z);
            file.Close();
            Debug.Log("Datos cargados");
        }

    }

}

[System.Serializable]

public class DataPersistencia
{

    public Punto posicionJugador;





}


[System.Serializable]

public class Punto
{

    public float x;
    public float y;
    public float z;


    public Punto(Vector3 p)
    {
        x = p.x;
        y = p.y;
        z = p.z;
    }

    public Vector3 aVector()
    {
        return new Vector3(x, y, z);
    }

}
