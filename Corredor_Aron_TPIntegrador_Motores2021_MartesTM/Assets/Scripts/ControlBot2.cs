﻿using UnityEngine;

public class ControlBot2 : MonoBehaviour
{
    private int hp;
    private GameObject player;
    public int rapidez;

    void Start()
    {
        hp = 100;
        player = GameObject.Find("Player1");
    }

    private void Update()
    {
        transform.LookAt(player.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Proyectil"))
        {
            recibirdaño();
        }
    }
    public void recibirdaño()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }
    private void desaparecer()
    {
        Destroy(gameObject);
    }
}