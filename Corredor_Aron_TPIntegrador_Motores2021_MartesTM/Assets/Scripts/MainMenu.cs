﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene(1);

    }

    public void QuitarJuego()
    {
        Application.Quit();
        Debug.Log("Espere... Saliendo del juego.");
    }
}

