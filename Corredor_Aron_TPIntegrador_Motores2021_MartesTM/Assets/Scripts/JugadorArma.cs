﻿using System.Collections.Generic;
using UnityEngine;

public class JugadorArma : MonoBehaviour
{
    public List<CamaraArma> armainicial = new List<CamaraArma>();

    public Transform AimPosition;

    public Transform DefaultPosition;

    public Transform Weapon;

    public int activeWeaponIndex { get; private set; }

    private CamaraArma[] weaponSlots = new CamaraArma[1];

    void Start()
    {
        activeWeaponIndex = -1;

        foreach (CamaraArma armainicial in armainicial)
        {
            AddWeapon(armainicial);
        }
    }


    void Update()
    {

    }

    private void AddWeapon(CamaraArma p_weaponPrefab)
    {
        Weapon.position = DefaultPosition.position;

        for (int i = 0; i < weaponSlots.Length; i++)
        {
            if (weaponSlots[i] == null)
            {
                CamaraArma weaponClone = Instantiate(p_weaponPrefab, Weapon);
                weaponClone.gameObject.SetActive(false);

            }
        }
    }
}
