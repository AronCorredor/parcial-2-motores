﻿using UnityEngine;

public class DestruirPared : MonoBehaviour
{
    private int hp;
    public GameObject TiroAlBlanco;

    void Start()
    {
        hp = 100;
    }

    private void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Proyectil"))
        {
            recibirdaño();
        }
    }
    public void recibirdaño()
    {
        hp = hp - 100;

        if (hp <= 0)
        {
            Destroy(TiroAlBlanco);
        }
    }

}
