﻿using UnityEngine;
using UnityEngine.UI;

public class Persistencia : MonoBehaviour
{
    public Text Cronometro;
    public float numCronometro;

    public Text textorecord;

    private void Start()
    {
        numCronometro = 0;
        textorecord.text = PlayerPrefs.GetFloat("PuntajeRecord", 0f).ToString();
    }
    void Update()
    {

        puntajetime();
    }
    public void puntajetime()
    {
        numCronometro += Time.deltaTime;
        Cronometro.text = "" + numCronometro.ToString();
        PlayerPrefs.SetFloat("PuntajeRecord", numCronometro);

    }
}
