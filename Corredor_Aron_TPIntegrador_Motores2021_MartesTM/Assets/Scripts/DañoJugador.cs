﻿using UnityEngine;
using UnityEngine.UI;

public class DañoJugador : MonoBehaviour
{
    private int hp;
    public Text textoPerdiste;
    public Text textoHP;
    public Rigidbody rb;
    public Cronometro cronometro;

    void Start()
    {
        hp = 100;
    }

    void Update()
    {
        if (hp == 0)
        {
            rb.GetComponent<Rigidbody>().position = new Vector3(42f, 0f, -3f);
        }
        setearTextos2();
        if (hp == 0)
        {
            cronometro.enabled = false;
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("BalaBot"))
        {
            recibirdaño();
        }
        if (collision.gameObject.CompareTag("Shadergraph"))
        {
            recibirdaño2();
        }
    }

    public void recibirdaño()
    {
        hp = hp - 10;

        if (hp == 0)
        {
            rb.GetComponent<Rigidbody>().position = new Vector3(42f, 0f, -3f);
        }
        setearTextos2();
    }
    public void recibirdaño2()
    {
        hp = hp - 100;

        if (hp == 0)
        {
            rb.GetComponent<Rigidbody>().position = new Vector3(42f, 0f, -3f);
        }
        setearTextos2();
    }
    private void setearTextos2()
    {
        if (hp <= 0)
        {
            textoPerdiste.text = "Mala suerte, Perdiste! Apreta R para volver a empezar!";
        }
        textoHP.text = "HP: " + hp.ToString();
    }

}

