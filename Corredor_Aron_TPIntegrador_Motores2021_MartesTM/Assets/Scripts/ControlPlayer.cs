﻿using UnityEngine;
using UnityEngine.UI;

public class ControlPlayer : MonoBehaviour
{
    [Header("Informacion Desplazamiento")]
    public float rapidezDesplazamiento = 10.0f;

    [Header("Informacion Partida")]
    public Camera camaraPP;
    public GameObject PokeBall;
    public Text contadorPokeBalls;
    public Text puertaAbierta;
    public CapsuleCollider col;
    public GameObject ParedBloqueo3;
    public float magnitudSalto;
    public LayerMask capaPiso;
    private Rigidbody rb;

    [Header("Informacion Saltos")]
    public int maxSaltos = 2;
    public int saltoActual = 0;
    private bool enElSuelo = true;
    private int cont;

    [Header("Coordenadas")]
    float x;
    float y;
    float z;

    [Header("Sonido")]
    public AudioSource controlSonido;
    public AudioClip sonidoMusica;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        Time.timeScale = 1;
        controlSonido.PlayOneShot(sonidoMusica);
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);

        }

        if (Input.GetButtonDown("Jump") && (enElSuelo || maxSaltos > saltoActual))
        {
            rb.velocity = new Vector3(0f, magnitudSalto, 0f * Time.deltaTime);
            enElSuelo = false;
            saltoActual++;
        }
        if (cont >= 5)
        {
            Destroy(ParedBloqueo3);
            mostrarTexto();
            desaparecerTexto();
        }

    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void OnCollisionEnter(Collision collision)
    {

        enElSuelo = true;
        saltoActual = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PokeBall") == true)
        {
            cont = cont + 1;
            contarPokeballs();
            other.gameObject.SetActive(false);

        }
        if (other.gameObject.CompareTag("ActivadorTexto2"))
        {
            contarPokeballs();
        }
    }
    private void contarPokeballs()
    {
        contadorPokeBalls.text = "AGARRASTE: " + cont.ToString() + " Pokeballs!";
    }

    private void mostrarTexto()
    {
        puertaAbierta.text = "PUERTA ABIERTA!";
    }

    private void desaparecerTexto()
    {
        Destroy(contadorPokeBalls);
    }
}
