﻿using UnityEngine;

public class LaserLoop : MonoBehaviour
{
    private int hp;
    bool tengoQueBajar = false;
    public int rapidez;

    void Start()
    {

    }

    private void Update()
    {
        if (transform.position.y >= 99.5f)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 87.85f)
        {
            tengoQueBajar = false;
        }
        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}

