﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlOjos : MonoBehaviour
{
    private int hp;
    public Text textoHPBoss;
    public GameObject DestruirPared;

    void Start()
    {
        hp = 125;
        textoHPBoss.enabled = false;
    }

    
    void Update()
    {
        if (HabilitarBot.activoAlBoss == true)
        {
            textoHPBoss.enabled = true;
        }
        setearTexto();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Proyectil"))
        {
            recibirdaño();
        }
    }
    public void recibirdaño()
    {
        hp = hp - 25;

        if (hp <= 0)
        {
            this.desaparecer();
        }
        setearTexto();
    }
    private void desaparecer()
    {
        Destroy(gameObject);
        Destroy(DestruirPared);
    }

    private void setearTexto()
    {
        textoHPBoss.text = "VIDA BOSS: " + hp.ToString();
    }
}
