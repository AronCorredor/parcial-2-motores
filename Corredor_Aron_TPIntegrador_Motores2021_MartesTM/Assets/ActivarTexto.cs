﻿using UnityEngine;
using UnityEngine.UI;

public class ActivarTexto : MonoBehaviour
{
    public Text textoInfo;
    public Text textoInfoBase;

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            mostrarTexto();
            ocultarTexto();
        }
    }

    private void mostrarTexto()
    {
        textoInfo.text = "Encontra el Blanco correcto para abrir la Puerta!";
    }
    private void ocultarTexto()
    {
        Destroy(textoInfoBase);
    }
}
