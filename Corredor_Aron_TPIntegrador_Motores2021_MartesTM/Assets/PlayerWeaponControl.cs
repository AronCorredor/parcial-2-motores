﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponControl : MonoBehaviour
{
    public List<CamaraArma> armainicial = new List<CamaraArma>();

    public Transform AimPosition;

    public Transform DefaultPosition;

    public Transform Weapon;

    public GameObject Pistola;

    public GameObject PistolaCuerpo;

    private bool enZona;

    private bool activa;

    public int activeWeaponIndex { get; private set; }

    private CamaraArma[] weaponSlots = new CamaraArma[1];

    void Start()
    {
        activeWeaponIndex = -1;

        foreach (CamaraArma armainicial in armainicial)
        {
            AddWeapon(armainicial);
        }
    }


    void Update()
    {

    }

    private void AddWeapon(CamaraArma p_weaponPrefab)
    {
        Weapon.position = DefaultPosition.position;

        for (int i = 0; i < weaponSlots.Length; i++)
        {
            if (weaponSlots[i] == null)
            {
                CamaraArma weaponClone = Instantiate(p_weaponPrefab, Weapon);
                weaponClone.gameObject.SetActive(false);

                weaponSlots[i] = weaponClone;
                return;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Pistola")
        {
            Pistola.SetActive(false);
            PistolaCuerpo.SetActive(true);
            Destroy(Pistola);
        }
    }

}
